---
layout: page
title: 有趣的
permalink: /ideas/
---

* [ST3主题切换](https://github.com/aaronzjc/Personal_Toys/tree/master/Sublime)
* [&lt;Silicon Valley&gt; Tracker](https://github.com/aaronzjc/Personal_Toys/tree/master/SeriesNotify)
* [N子棋](https://github.com/aaronzjc/Personal_Toys/tree/master/N%20cheese)
* [Todo提醒](https://github.com/aaronzjc/Personal_Toys/tree/master/Note)
* [Vue-Chat](https://github.com/aaronzjc/vue-chat)
